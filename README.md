## Usage

1. Clonar codigo proyecto.

```bash
git clone  my-project
cd my-project
```

2. Instalando dependencias.

```bash
yarn install
```

O

```bash
npm install
```

3. Iniciando servidor local.

```bash
npm run start
```

